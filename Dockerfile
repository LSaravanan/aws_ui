#Build Stage
FROM alpine as build-env

WORKDIR /ui

COPY . .

RUN apk add --update nodejs npm && \
    npm install -g @angular/cli

RUN npm install

RUN ng build --outputPath /deployment


# Runtime Stage
FROM nginx:alpine
COPY --from=build-env /deployment /usr/share/nginx/html
