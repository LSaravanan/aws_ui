import { Injectable } from '@angular/core';
import { ITrip } from '../Trip/Trip';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import { SecurityService } from './security.service';
import { AppUserAuth } from '../security/Model/app-user.auths';
import { TripDetail } from '../Trip/Model/trip-detail';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

@Injectable({
    providedIn: 'root'
})
export class bookingService{

    securityObject : AppUserAuth = null;

constructor(private http: HttpClient, securityService: SecurityService){
    this.securityObject = securityService.securityObject;
}

private tripserviceUrl = 'http://tmserviceapi-test.us-east-1.elasticbeanstalk.com/api/trip/';
private vehicleServiceUrl = 'http://tmserviceapi-test.us-east-1.elasticbeanstalk.com/api/vehicle/'

getAllTrips(): Observable<ITrip[]>{

    let url = this.tripserviceUrl;

    return this.http.get<ITrip[]>(url).pipe(
        tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
    );
}

getTripsByCustomerId(id:string): Observable<ITrip[]>{

    let url = this.tripserviceUrl + 'customer/' + id;

    return this.http.get<ITrip[]>(url).pipe(
        tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
    );
}

getTripId(id: string): Observable<ITrip>{

    let url = this.tripserviceUrl + id;

    return this.http.get<ITrip>(url).pipe(
        tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
    );
}

AddTrip(tripdetail: TripDetail): Observable<void>{

    let url = this.tripserviceUrl;

    return this.http.post<void>(url,tripdetail,httpOptions).pipe(
        catchError(this.handleError)
    );
}

CancelTrip(id: string): Observable<void>{

    let url = this.tripserviceUrl + id;

    return this.http.delete<void>(url).pipe(
        catchError(this.handleError)
    );
}

ConfirmTrip(tripid: string, employeeid: string): Observable<void>{

    let url = this.tripserviceUrl + tripid + '/employee/' + employeeid;

    return this.http.put<void>(url,{trid:tripid, empid:employeeid},httpOptions).pipe(
        catchError(this.handleError)
    );
}

//Add edit and update Vehicle information



//End CRUD for vehicle


private handleError(err: HttpErrorResponse){
    let errorMessage = '';
    if(err.error instanceof ErrorEvent){

    }
    else{
        errorMessage = `Server returned code: ${err.status}, error message is ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
}

}