import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { TripComponent } from './Trip/Trip.component';
import { TripDetailsComponent } from './Trip/trip-details.component';
import {RouterModule} from '@angular/router';
import { VehicleComponent } from './Vehicle/vehicle.component';
import { LoginComponent } from './security/view/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './security/auth.guard';
import { HttpInterceptorModule } from './security/http-interceptor';
import { HasClaimDirective } from './security/has-claim.directive';

@NgModule({
  declarations: [
    AppComponent,
    TripComponent,
    TripDetailsComponent,
    VehicleComponent,
    LoginComponent,
    HomeComponent,
    HasClaimDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpInterceptorModule,
    RouterModule.forRoot([
      {
        path: 'home', 
        component: HomeComponent
      },
      {
        path: 'login', 
        component: LoginComponent
      },
      {
        path: 'booktrip', 
        component: TripComponent,
        canActivate: [AuthGuard],
        data: {claimType: ['IsCustomer','IsEmployee','IsAdmin']}
      },
      {
        path: 'Vehicle', 
        component: VehicleComponent,
        canActivate: [AuthGuard],
        data: {claimType: ['IsEmployee']}
      },
      {
        path: 'booktrip/:id', 
        component:TripDetailsComponent,
        canActivate: [AuthGuard],
        data: {claimType: ['IsCustomer','IsEmployee']}
      },
      {path: '', redirectTo:'login', pathMatch: 'full'},
      {path:'**', redirectTo: 'login', pathMatch:'full'}
    ],{useHash:false})
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
