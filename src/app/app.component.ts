import { Component } from '@angular/core';
import { AppUserAuth } from './security/Model/app-user.auths';
import { SecurityService } from './sharedService/security.service';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  securityObject : AppUserAuth = null;

  
  title = 'Booking Service';

  constructor(private securityService: SecurityService,private router: Router){
    this.securityObject = securityService.securityObject;
  }

  logout():void{
    this.securityService.logout();
    this.router.navigate(['/customer']);
  }

}
