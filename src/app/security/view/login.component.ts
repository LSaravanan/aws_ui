import { Component, OnInit, NgZone } from '@angular/core';
import { AppUser } from '../Model/app-user';
import { AppUserAuth } from '../Model/app-user.auths';
import { SecurityService } from 'src/app/sharedService/security.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'pm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: AppUser = new AppUser();
  securityObject: AppUserAuth = null;
  returnUrl: string;


  constructor(private securityService: SecurityService, 
    private router: Router, 
    private route: ActivatedRoute,
    private ngZone: NgZone) { }

  login(){
    this.securityService.login(this.user).subscribe(
      resp => {
        this.securityObject = resp;
       
        if(this.returnUrl){

          this.ngZone.run(() => {
            this.router.navigateByUrl(this.returnUrl);
          });         
        }
        else{
          this.ngZone.run(() => {
            this.router.navigate(['/home']);
          }); 
        }
      },
      ()=>{
        this.securityObject = new AppUserAuth();
      }
    );
  }

  ngOnInit() {
    
    this.returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    console.log(this.returnUrl);
  }

}
