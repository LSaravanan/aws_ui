import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SecurityService } from '../sharedService/security.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate{

constructor(private securityService: SecurityService, private router: Router){

}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     


      let claimType: string[] = next.data["claimType"];

      // if (this.securityService.securityObject.isAuthenticated
      //   && this.securityService.hasClaim(claimType)) {
      //   return true;
      // }
      // else {
      //    this.router.navigate(['login'],
      //      { queryParams: { returnUrl: state.url } });
      //   return false;
      // }

      if (this.securityService.securityObject.isAuthenticated
          && this.checkGuardList(claimType)) {
          return true;
        }
        else {
           this.router.navigate(['login'],
             { queryParams: { returnUrl: state.url } });
          return false;
        }


    }

    private checkGuardList(guardlist: string[]): boolean
    {
      let ret = false;

      guardlist.forEach(element => {
        if(this.securityService.hasClaim(element))
          ret = true;
      });

      return ret;
    }
  
 
}
