import { AppUserClaim } from './app-user-claim';

export class AppUserAuth {
    id: string="";
    userName: string="";
    bearerToken: string ="";
    isAuthenticated: boolean = false;

    claims: AppUserClaim[] =[];
    
}