import { Component, OnInit } from '@angular/core';
import { ITrip } from './Trip';
import { bookingService } from '../sharedService/Booking.services';
import { AppUserAuth } from '../security/Model/app-user.auths';
import { SecurityService } from '../sharedService/security.service';
import { Router } from '@angular/router';


@Component({
    selector: 'pm-customer',
    templateUrl : './Trip.component.html',
    styleUrls: ['./Trip.component.css'],
})
export class TripComponent implements OnInit{

    securityObject: AppUserAuth = null;

    constructor(private customerService: bookingService, 
        private securityService: SecurityService,
        private router: Router){

        this.securityObject = securityService.securityObject;
    }

    pageTitle: string = "Trip List"; 
    errorMessage: string='';


   

    trips: ITrip[] = [];

    ngOnInit():void{

    

        var IsCustomer =  this.securityObject.claims.find(c =>
            c.claimType.toLowerCase() == 'IsCustomer'.toLowerCase() &&
            c.claimValue == 'true') != null;
        var IsEmployee =  this.securityObject.claims.find(c =>
                c.claimType.toLowerCase() == 'IsEmployee'.toLowerCase() &&
                c.claimValue == 'true') != null;
        var IsAdmin =  this.securityObject.claims.find(c =>
                    c.claimType.toLowerCase() == 'IsAdmin'.toLowerCase() &&
                    c.claimValue == 'true') != null;

        if(IsCustomer)
        {
            this.customerService.getTripsByCustomerId(this.securityObject.id).subscribe({
                next: trips => {
                    this.trips = trips
                },
                error: err => this.errorMessage = err
            });
        }
        else if(IsEmployee || IsAdmin)
        {
            this.customerService.getAllTrips().subscribe({
                next: trips => {
                    this.trips = trips
                },
                error: err => this.errorMessage = err
            });
        }
    }

    onSave():void{
        //write code to save changes
    }

    bookTrip():void{
        this.router.navigate(['/booktrip/book']);
    }
}