export interface ITrip{
    id: number,
    sourceLocation: string,
    destinationLocation: string,
    time: string,
    vehicleid: string,
    status: string
}
