import { Component, OnInit, NgZone } from '@angular/core';
import { ITrip } from './Trip';
import { ActivatedRoute, Router} from '@angular/router';
import { bookingService } from '../sharedService/Booking.services';
import { AppUserAuth } from '../security/Model/app-user.auths';
import { TripDetail } from './Model/trip-detail';
import { SecurityService } from '../sharedService/security.service';


@Component({
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.css']
})
export class TripDetailsComponent implements OnInit {

  securityObject: AppUserAuth = null;
  errorMessage: string='';
  enableAdd: boolean = false;
  tripid: string="0";

  _sourceLocation: string;
  get sourceLocation(): string{
      return this._sourceLocation;
  }
  set sourceLocation(value: string) {
      this._sourceLocation = value;
      
  }

  _destinationLocation: string;
  get destinationLocation(): string{
      return this._destinationLocation;
  }
  set destinationLocation(value: string) {
      this._destinationLocation = value;
      
  }

  _vehicleid: string;
  get vehicleid(): string{
      return this._vehicleid;
  }
  set vehicleid(value: string) {
      this._vehicleid = value;
      
  }

  _time: string;
  get time(): string{
      return this._time;
  }
  set time(value: string) {
      this._time = value;
      
  }
  

  constructor(private route: ActivatedRoute, private router: Router, 
    private bookingService: bookingService,
    private securityService: SecurityService,
    private ngZone: NgZone){
    this.securityObject = securityService.securityObject;

    console.log(this.route.snapshot.paramMap.get('id'));
   }

  pageTitle: string = 'Book Details'
  trip: ITrip;

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.tripid = id;
debugger;
    if(id!="book"){
      this.enableAdd = false;
    this.bookingService.getTripId(id).subscribe({
      next: trip => {
          this.trip = trip
          this.sourceLocation = this.trip.sourceLocation;
          this.destinationLocation= this.trip.destinationLocation;
          this.time = this.trip.time;
          this.vehicleid = this.trip.vehicleid;

              },
              error: err => this.errorMessage = err
          });
    }
    else
    {
      this.enableAdd = true;
    }
  }

  onBack():void{
    this.router.navigate(['/booktrip']);
  }

  cancelTrip(): void{

    this.bookingService.CancelTrip(this.tripid).subscribe(
      res=>{
        //console.log(res);
        this.ngZone.run(() => {
          this.router.navigate(['/booktrip']);
        });
        
      }
    );
    
  }

  addTrip():void{
    debugger;
    let trip = new TripDetail();
    trip.DestinationLocation = this.destinationLocation;
    trip.SourceLocation = this.sourceLocation;
    trip.Time=this.time;
    trip.customerid=this.securityObject.id;
    trip.vehicleid=this.vehicleid;

    this.bookingService.AddTrip(trip).subscribe(
      res=>{
        //console.log(res);

        this.ngZone.run(() => {
          this.router.navigate(['/booktrip']);
        });
        
      }
    );

   
  }

  confirmTrip():void{
    this.bookingService.ConfirmTrip(this.tripid,this.securityObject.id).subscribe(
      res=>{
        //console.log(res);
        this.ngZone.run(() => {
          this.router.navigate(['/booktrip']);
        });
        
      }
    );
  }



}
